.PHONY: all db migrate kill-db

# The "all" rule executes the "db" and "migrate" rules
all: db migrate

# The "db" rule brings up Docker containers in the background
db:
	docker-compose up -d
	touch .db
	@echo "db_service" >> .db
	@echo "Docker containers are up and running."

# The "migrate" rule initializes the database after the containers are up and running
migrate: .db
	@pnpm db:init
	@echo "Database migration completed successfully."

pull: .db
	@echo "Pulling schema."
	@pnpm db:pull
	@node read.ts
	#do a migrate to name the migration
	#@pnpm migrate
# The "kill-db" rule stops and removes the containers if they exist and cleans the ".db" file
kill-db:
	docker-compose stop
	docker-compose down
	rm .db

restart-db:
	@echo "resetting database."
	./restart.sh

