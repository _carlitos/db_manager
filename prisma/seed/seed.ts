/**
 * ! Executing this script will delete all data in your database and seed it with 10 versions.
 * ! Make sure to adjust the script to your needs.
 * Use any TypeScript runner to run this script, for example: `npx tsx seed.ts`
 * Learn more about the Seed Client by following our guide: https://docs.snaplet.dev/seed/getting-started
 */
import { createSeedClient } from "@snaplet/seed";
import { PrismaClient } from "@prisma/client";
import * as fs from 'fs';

const main = async () => {
  const seed = await createSeedClient();

  // Truncate all tables in the database
  //await seed.$resetDatabase();


  // Seed the database with 10 users
  //await seed.user((createMany) => createMany(10, {
  // Create 10 posts for each of those users
  //posts: (createMany: any) => createMany(10),

  // }))

  console.log("Database seeded successfully!");

  process.exit();
};

function loadJson(path: string): Promise<any> {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) {
        reject(err);
        return;
      }
      try {
        const json = JSON.parse(data);
        resolve(json);
      } catch (parseError) {
        reject(parseError);
      }
    });
  });
}
const loadData = async () => {
  const all = await loadJson("state.json")
  return all

}

////main();kjkkk
const main2 = async () => {
  const prisma = new PrismaClient()
  const data2Insert = await loadData();
  const entityKeys = Object.keys(data2Insert)
  const mappedPromises: Promise<string>[] = entityKeys.map((key => {
    return new Promise(async (resol, reje) => {
      const modeKeyWithType = key as keyof typeof prisma;
      const instance = prisma[modeKeyWithType];
      const createMany = "createMany" as keyof typeof instance;
      const createManyinstance = instance[createMany] as Function

      const mapped: Object[] = data2Insert[key].map((item: Object) => ({ ...item }))

      if (mapped.length === 0) reje("Empty array.");
      resol(await createManyinstance({ data: mapped, skipDuplicates: true }))

    })
  }))
  const done = Promise.allSettled(mappedPromises)
  console.log(await done)
}
main2();


