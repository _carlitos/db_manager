import { PrismaClient } from "@prisma/client";
import * as fs from 'fs';


const prismaClient = new PrismaClient()
function saveJson(path: string, data: any): Promise<void> {
  return new Promise((resolve, reject) => {
    const json = JSON.stringify(data, null, 2);
    fs.writeFile(path, json, 'utf8', (err) => {
      if (err) {
        reject(err);
        return;
      }
      resolve();
    });
  });
}

async function main() {
  const models = Object.keys(prismaClient).filter((modelKey) => {
    return !modelKey.includes("$", 0) && !modelKey.includes("_", 0)
  })

  const dbCopy = {}
  let entities: Record<string, Partial<any>> = {}
  // "p1": { firstName: "F1", lastName: "L1" },
  // "p2": { firstName: "F2" }
  // };
  //console.log("models", models, prismaClient)
  for (let index = 0; index < models.length; index++) {
    const model = models[index]

    console.log("current model: ", model)
    if (model === "Post") return
    let str = model as keyof typeof prismaClient;

    console.log({ str })
    const instance = prismaClient[str]
    const findManyToken = "findMany" as keyof typeof instance;
    const modelInstance = instance[findManyToken] as Function;
    const all = await modelInstance()
    const prismaKeyStr = str.toString()
    entities[prismaKeyStr] = [...all]
  }
  await saveJson("./state.json", entities)
  console.log(entities)
}

main();
