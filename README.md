
# Database Manager

This project provides a convenient way to manage your database using Docker containers and Prisma ORM.

## Prerequisites

Before getting started, ensure you have the following installed on your system:

- Docker
- Node.js (with npm or pnpm)

## Getting Started

1. Clone this repository to your local machine:

   ```bash
   git clone <repository_url>
   ```

2. Navigate into the project directory:

   ```bash
   cd <project_directory>
   ```

3. Install project dependencies:

   If you use npm:

   ```bash
   npm install
   ```

   If you use pnpm:

   ```bash
   pnpm install
   ```

## Usage

### Starting the Database

To start the database, run the following command:

```bash
make db
```

This command will bring up Docker containers in the background with PostgreSQL and Adminer (a database management tool). The database will be initialized automatically.

### Migrating the Database

To migrate the database schema, run the following command:

```bash
make migrate
```

This command will apply any pending migrations to the database.

### Seeding the Database

If you need to seed the database with initial data, you can run:

```bash
pnpm run db:seed
```

This will execute the seed script defined in `prisma/seed/seed.ts`.

### Pulling Database Schema

If you want to pull the database schema from an existing database, or update your prisma.schema you can run:

```bash
pnpm run db:pull
```
then 

```bash
pnpm run migrate
```
in order to create the migration file.
note: the command will request you a migration name, try to add a descriptive one

### Cleaning up

To stop and remove the database containers and clean up any related files, you can run:

```bash
make kill-db
```

## Configuration

- The database configuration is specified in the `docker-compose.yml` file.
- The database schema and migrations are managed using Prisma ORM. The configuration is in the `schema.prisma` file.
- Environment variables for the database configuration can be set in the `.env` file.



## License